
\chapter{Introduction} \label{chap:intro}

When developing software, engineers often attempt to achieve correctness. Many companies adopt testing to reach at least some correctness, but those tests usually cover only elementary scenarios and omit corner or non-intuitive cases. Moreover, considering parallel programs, achieving adequate coverage is often impossible. 


Many techniques have been developed to resolve the problems of testing, such as symbolic execution \cite{SymbolicExecution}, model checking\cite{ModelChecking} or fuzzing\cite{fuzzingBook}. They all try to check the behaviour of the program automatically employing different strategies. 

The technique we will be working with is explicit-state model checking, employed to verify a program against a given property by analysing all possible thread interleavings of the program. Moreover, we will focus on applications that interact with the environment. These programs are often hard to verify using a model checker, as the model checker would have to check all possible inputs -- that introduces an enormous state space to explore. 

A few explicit-state model checking extensions are aiming to overcome this problem, such as abstraction-based model checking \cite{ModelCheckingAbstraction} and symbolic model checking \cite{ModelCheckingSymbolic}. They try to group a large number of possible inputs into a small number of groups that preserve program behaviour and then verify the behaviour of the program only using representatives. This approach may significantly reduce the size of the state space to explore. 


In \cite{Kejstova2017thesis}, I introduced a different approach, exploiting that many of these interactions with the outer environment are done by system calls. I implemented a mechanism capable of obtaining all system calls executed by a program during one run in the form of a trace, and use this trace for subsequent verification of the program in an internal environment. 


However, the state space covered by this approach was limited to executions that correspond to the original system call trace. In the case of parallel programs, this constraint can cause significant limitations.


In this thesis, I will aim to enlarge the possible state space covered during verification of such a program. I will attempt to achieve this using two different strategies. Firstly, I will redefine the original constraint on system calls and allow some of them to commute. Secondly, I will try to employ fuzzing to modify the trace and discover new, possibly erroneous traces in the program under test.    


The structure of this thesis is the following: Chapter \ref{chap:prelim} provides the essential fundamentals of crucial parts of UNIX-like operating systems and its file system structure. Subsequently, chapter \ref{chap:divine} provides a detailed description of \divine's components essential for our implementation. Chapter \ref{chap:fuzzy} presents fuzzing as a testing technique and presents some fuzzers aimed at system calls and their approach to data modification. 

Chapter \ref{chap:commuting} presents the definition of a new constraint for system call traces, a sketch of the implementation and some graphs that display a difference in resulting state space coverage.

Chapter \ref{chap:divinefuzzy} presents my approach taken for fuzzing traces for \divine and the overall workflow of this approach. Some results of applying the fuzzing technique on programs are described in chapter \ref{chap:results}, where I compare various approaches taken for fuzzing and selecting best traces for \divine, present some measurements and discuss the provided results. 
Finally, chapter \ref{chap:conclusion} summarises my contribution and discusses possible future work.

