\chapter{ Fuzzing Approach in Divine } \label{chap:divinefuzzy}
\
In this chapter, I describe the approach I have taken when implementing fuzzing in \divine.

\section{Definition of a Problem}

When executing programs in \divine (\texttt{exec} mode), the virtual machine propagates system calls through \dios into the real operating system and
records all information gained about those system calls in the form of captured traces.
During model checking, \divine can then load these traces back and use them to simulate system calls requested by the program under test.

However, currently only executions where these system calls commute with respect to \textit{inodes} (\textit{Chap} \ref{chap:commuting}) can be explored by \divine.
To make the model checker more flexible and thus able to explore a wider spectrum of program traces, I decided to examine
fuzzing technique on these recorded system call traces.

\section{Fuzzing system calls}

In \textit{Sec} \ref{chap:fuzzy_syscall}, I described the approach taken for fuzzing system calls by some fuzzers, namely \textit{Trinity} and \textit{perf\_fuzzer}. My approach, however, targets the program under test rather than the operating system (\dios) thereby it is unreasonable to exploits the whole idea of the system calls fuzzing. 

Firstly, it makes no sense to fuzz both input and output parts of a system call (e.g. those filled by user and those filled by the operating system). I fuzz only those parts which common system call fuzzer would not.
That naturally emerge from the fact that I would like to modify only those part of a system calls that are read by the program and therefore determine the program's behaviour.
Fuzzing parts filled by the program is unreasonable, does not shape the behaviour of a program and also decreases the probability of matching some system call trace to a real call of the program under the test, as the system call is usually matched by the parts filled by the program.

Secondly, it may be reasonable to fuzz return values of system call as well as \texttt{errno}. The reason is similar to described above, both return value as well as \texttt{errno} may influence the behaviour of a program. The range of values for fuzzing return value and \texttt{errno} should not be entirely random. The fully random value may be out of the valid range described by the standard and therefore produce only false positive or undefined behaviour and no real errors.

For \texttt{errno}, the reasonable range is the scope of values \texttt{errno} can actually take. Even this approach can be described as over-approximating as some system calls can take only a subset of those values. For example, system call \texttt{open} cannot cause the \texttt{errno} to have a value \texttt{EAGAIN} \cite{Man_open}.

Finally, there is no reason to use only entirely random data for other parts of system calls as done by general system call fuzzers \cite{Trinity}, \cite{perf_fuzzer}. I can exploit the data provided by the original system call trace and thus offer partially random input. It is also possible to duplicate various system calls and observe the effect it causes. In general, the possibilities in case of fuzzing traces can benefit from being biased towards the original trace, more so than in the case of general fuzzing.

\section{Specific types}

As already mentioned, we can differentiate between various data types when fuzzing system calls. There are return values and \texttt{errno} with specific range requirements, and we can also separate strings inputs from structures. I have taken a different approach to fuzzy these types. 

\subsection{Strings}

In some system call fuzzers, upon requesting a string, a sequence of random bytes is generated. These bytes are be obtained by different techniques of creating a random sequence of bytes or by reading from various files (from directories such as \texttt{/dev}, \texttt{/proc} or \texttt{/sys} \cite{Trinity}).

I have taken a slightly different approach. The string can be both filled by random data, or its modification may be based on the string content itself.

To generate random data I used these techniques:

\begin{itemize}    
\item{a sequence of fully random bytes}
\item{a sequence of fully random bytes in the range of printable characters}
\item{some sequence from various files, namely \texttt{/proc/sysrq-trigger}, \texttt{/proc/kmem"}, \texttt{/proc/kcore}, \texttt{/dev/log}, \texttt{/dev/mem},  \texttt{/dev/kmsg}, \texttt{/dev/kmem}, as they may contain noisy, but not fully random, data}
\item{a sequence of \texttt{0x00} or a sequence of \texttt{0xff} }
\end{itemize}

To generate a fuzzed string based on the original content I used:

\begin{itemize}    
\item{a substring repeated alongside the original string }
\item{random string permutation of its characters}
\item{sorted form of the original string} 
\end{itemize}

 This approach is advantageous in moments when the program expects output in some specific format, or that the output contains some specific characters. Here an entirely random string may have a smaller probability of deviating the program than some modification of its original structure.

\subsection{Return}

As already mentioned, fuzzing of return values cannot occur entirely random. In general, the value $-1$ refers to an unsuccessful system call (\textit{Chap.} \ref{chap:prelim:errno}).
We distinguish three types of system calls: The first group returns $0$ in case of success. These system calls can return only $-1$ or $0$. The second group can return an integer value as a success. An example is the system call \texttt{write} which returns the number of characters written.
The third group returns a non-integer value; for example, the system call \texttt{getcwd}.

The implementation alters only integer values, and the third group is omitted from fuzzing. In the second group, we fuzz return value in the range $[-1, x]$ where x is the value returned initially according to the captured trace. Setting $x$ to a value that is bigger than the original can cause the program to access either undefined data or data beyond allocated space through no fault of its own, causing false positive.

\subsection{Errno}

As already mentioned, the fuzzing of \texttt{errno} values cannot happen entirely random also. The value of \texttt{errno} should correspond to one of the constants defined in the header file \texttt{errno.h}. Therefore, the valid range of \texttt{errno} while fuzzing is exactly constants defined in this header. At this moment there is a guarantee that the modified value of \texttt{errno} will always be at least a valid constant. 

\subsection{Aggregate data}

In case of fuzzing structures subsequently forwarded to the program under test, we may not be so benevolent as in the case of strings.
A counterexample can be structure holding a pointer to some program allocated memory. 

 While fuzzing a data aggregation as an input for an operating system, you may want to fuzz the value of a pointer in the hope that you crash the operating system. On the other hand, while fuzzing a trace for \divine, you certainly do not want to do this, as this causes false positive. 

Data aggregation is often a complex unit, and we need to fuzz it with an awareness of its structure. I implemented different fuzzing strategies
 for \texttt{sockaddr} structures and for \texttt{stat} structure. \\ 

The \texttt{stat} structure has a purely informative purpose; therefore I fuzz its content similar to string. \\

By \texttt{sockaddr} structures, I mean a family of structure \texttt{sockaddr}. Their complete construction depends on its first attribute,
namely \texttt{sa\_family}, representing their real type. If this attribute has the value \texttt{AF\_INET}, the structure must be interpreted as \texttt{sockaddr\_in}.
On the other hand, if the \texttt{sa\_family} equals \texttt{AF\_UNIX} the structure must be interpreted as \texttt{sockaddr\_un}.

The complex format of this structure makes it unpleasant for fuzzing.
I decided to use the already defined random bytes generator for entire structure but retain the original \texttt{sa\_family} value.

As this structure is often used for informative purposes only or is subsequently forwarded to another system call (\texttt{sendto}),
its alternation is either quite unlikely to generate an error or will cause a failure to find matching system call in the system call trace.
Therefore, structure fuzzing is performed with smaller probability than other fuzzing, and when it is done, I replace all original structure occurrences with its altered version.

This way I increase the probability of finding matching system call from the recorded syscall trace.


\section{The Fuzzer}

\subsection{Workflow}

\begin{figure}[h!]
\begin{tikzpicture}[ ->, >=stealth', shorten >=1pt, auto, node distance=3cm
                   , semithick
                   , style={ node distance = 2em }
                   , state/.style={ rectangle, draw=black, very thick,
                     minimum height=1.7em, minimum width = 4.4em, inner
                     sep=2pt, text centered, node distance = 2em },
                         *|/.style={
        to path={
            (perpendicular cs: horizontal line through={(\tikztostart)},
                                 vertical line through={(\tikztotarget)})
            % is the same as (\tikztostart -| \tikztotarget)
            % but just to be safe: http://tex.stackexchange.com/a/29781/16595
            -- (\tikztotarget) \tikztonodes
        }
    },
    |*/.style={to path={
        (\tikztostart) -- (perpendicular cs: vertical line through={(\tikztostart)},
                                             horizontal line through={(\tikztotarget)})
    }}
                   ]
  \node[state, minimum width = 6em] (code) {C++ code};
  \node[state, minimum width = 6em, right = 0.6em of code] (divine) {\divine};
  %\node[state, minimum width = 10.4em, right = 13.6em of code] (prop) {property and options};

  \node[state, below = 2.8em of code, rounded corners] (passthru) {exec (passthrough)};
  \node[state, right = of passthru] (fuzz) {fuzz outputs};
  \node[state, right = of fuzz, rounded corners, minimum width = 8em] (draw) {draw};
  \node[state, below = 1.5em of draw] (graph) {process graphs};
  \node[state, below = 1.5em of fuzz, rounded corners, minimum width = 8em] (selection) {pick best graphs};
  \node[above = 0.5em of draw] (fround) {};

  \node[state, below = 1.7em of graph, minimum width = 8em] (ce) {\color{orangef}Errors};

  \begin{pgfonlayer}{background}
      \node[state, fit = (fround) (passthru) (fuzz) (draw) (graph) (selection),
            inner sep = 0.8em, thick, rounded corners, dashed] (verify) {};
  \end{pgfonlayer}

  \node[below = 0.2em] at (verify.north) {\texttt{fuzzer}};

  \path (code) edge (passthru)
          (divine) edge (passthru)
        (passthru) edge (fuzz)
        (fuzz) edge (draw)
        (draw) edge (graph)
        (graph) edge (selection) edge (ce)
        (selection) edge (fuzz)
        ;
\end{tikzpicture}
\caption{
        Approximate \divine syscall traces fuzzer work-flow.
    }
  \label{fig:fuzzer::workflow}
\end{figure}


The basic workflow of the fuzzer captured in \textit{Fig.} \ref{fig:fuzzer::workflow} is the following:  The inputs for the fuzzer are a path to a file with the C++ source code of a program that should be checked and \divine. 

After launching the fuzzer, the following steps are done (see \textit{Fig.} \ref{fig:fuzzer::workflow}): \textsf{exec (passthrough)} followed by a cycle of \textsf{fuzz outputs, draw, process graphs, pick best graphs}.

During \textsf{exec (passthrough)}, \divine runs the program in \texttt{exec} mode and produces a system call trace while propagating system calls to the underlying operating system. Note that this is the last moment in which the program communicates with the host operating system.  Subsequently, system calls are only simulated by \dios.

In the \textsf{fuzz outputs} phase, all currently available traces are investigated in order to fuzz their system calls.  Fuzzing of system calls generates new traces. Subsequently, in the \textsf{draw} phase, these new traces are used as an input for system call simulation in \divine.

 This means that \divine \texttt{draw} mode is executed in a slightly modified \texttt{replay} system calls mode, with a fuzzed trace as an input. This run generates a graph of all states reachable in the verified program with respect to the altered trace.

In \textsf{process graphs}, this generated graph is processed into an internal graph representation with additional information about the result of every system call requested by the program. Here we distinguish between a system call that \textit{succeeded}, and a one that \textit{failed}.

In the \textsf{pick best graphs} phase, a suitable subset is selected from the available graphs. This selection is used for the next iteration.

\subsection{Fuzzing}

\begin{figure}[h!]
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=1.5cm,
                    semithick]

  \tikzstyle{every state}=[fill=yellowf!60,draw=none,text=black,style=rectangle,rounded corners]

  \node[initial,state] (A)                    {[program]};
  \node[state]         (B) [below = 1.5em of A] {passthrough.out};
  \node[state]         (C) [below = 1.5em of B] {graph of [program]};

  \node[state]         (D1) [below right of=C]       {trace};
  \node[state]         (D2) [below left of=C]       {trace};
  \node[state]         (D3) [left of=D2]       {trace};
  \node[state]         (D4) [right of=D1]       {trace};
  \node[state]         (D5) [right of=D4]       {trace};
  \node[state]         (D6) [left of=D3]       {trace};

  \node[state]         (E1) [below = 1.5em of D1]       {graph};
  \node[state]         (E2) [below = 1.5em of D2]      {graph};
  \node[state]         (E3) [below = 1.5em of D3]       {graph};
  \node[state]         (E4) [below = 1.5em of D4]      {graph};
  \node[state]         (E5) [below = 1.5em of D5]      {graph};
  \node[state]         (E6) [below = 1.5em of D6]      {graph};

  \node[state]         (F) [below = 12.5em of C]      {graph selection};


  \node[state]         (H2) [right = 10.1em of C, fill=white, scale = 0.05]      {};
  \node[state]         (H1) [right = 11.0em of F, fill=white, scale = 0.05]      {};



  \path (A) edge              node {record} (B)
        (B) edge              node {draw(replay)} (C)
        % FUZZY
        (C) edge              node {} (D1)
            edge              node {} (D2)
            edge              node {} (D3)
            edge              node {} (D4)
            edge              node {all: fuzz} (D5)
            edge              node {} (D6)
        % DRAW
        (D1) edge              node {draw} (E1)
        (D2) edge              node {draw} (E2)
        (D3) edge              node {draw} (E3)
        (D4) edge              node {draw} (E4)
        (D5) edge              node {draw} (E5)
        (D6) edge              node {draw} (E6)
        % SELECT
        (E2) edge              node {} (F)
        (E3) edge              node {} (F)
        (E4) edge              node {some: select} (F)
        % REPEAT
        (F) edge      [-]         node {} (H1)
        (H1) edge    [right,-]          node {foreach: repeat} (H2)
        (H2) edge              node {} (C)
        % (F) edge               node {repeat} (C)
        ;
  % \begin{pgfonlayer}{background}
  %     \node[state, fit = (D1) (D2) (D3) (D4) (D5) (D6),
  %           inner sep = 0.8em, thick, rounded corners, dashed, fill=white] (traces) {};
  % \end{pgfonlayer}


\end{tikzpicture}
\caption{
        Approximate fuzzer state space description.
    }
  \label{fig:fuzzer::fuzzing}
\end{figure}


With a closer look to the workflow of the fuzzer in \textit{Fig.} \ref{fig:fuzzer::fuzzing}, one can notice that there are two phases which can substantially (besides randomness itself) influence the outcome of the fuzzer. I call these phases  \textsf{fuzz} and  \textsf{select}.

The  \textsf{fuzz} phase is responsible of fuzzing traces. I will distinguish among these (\textsf{fuzz}) possibilities:

\begin{itemize}
\item {\textit{bits} -- fuzzing based on flipping bits in strings and structures}
\item {\textit{random} -- fuzzing strings and structures only using purely random data}
\item {\textit{string} -- fuzzing strings and structures only using either random data or content-based modification}
\item {\textit{return} -- fuzzing using approach described above combined with fuzzing also return value and \texttt{errno}}
\end{itemize}


The  \textsf{select} phase is responsible of selecting the best graphs for the next phase. I will distinguish among these (\textsf{select}) possibilities:

\begin{itemize}
\item {\textit{all} -- always select all the generated graphs }
\item {\textit{longest} -- select $n$ graphs with the longest branch }
\item {\textit{fanout} -- select $n$ graphs with the biggest fanout, e.g. out-degree }
\item {\textit{coverage} -- select $n$ graphs with the biggest coverage of executed system calls (counting both successful and failed calls) }
\end{itemize}

Where $n$ is constant during the entire run of fuzzer.

\subsection{Selection} \label{sec:fuzz_selection}

When we run \divine in a \texttt{draw} execution mode, we obtain a graphical representation of the generated state space. Three of my \texttt{select} methods work directly over these graphs. The fourth -- \textit{coverage}, works over system calls traces.

Considering a run of a program restricted to system calls only, we get a tree of system calls, which in the case of a non-parallel program reduces to a trace. A human-readable version of such a tree is presented in \textit{Fig.} \ref{fig:fuzzer::trie}.

If we represent each of these calls as a string, we naturally obtain a tree of strings. Now consider all paths starting in the root which finishes in some leaf -- they represent a set of all system calls traces executable with the given program and the given \texttt{passthrough} output.\\ 


When comparing \textsf{select} approaches, it is important to mention their different time complexity. While selecting \textit{All} traces for a next round is constant, determining \textit{Longest} property or \textit{Fanout} property of a graph takes linear time with respect to number of nodes. 

Finally, \textit{Coverage} problem is a well known NP-complete problem \cite{KarpNP}. Therefore I decided to implement its greedy version \cite{GreedyCoverage}. This greedy approach requires a set representation of traces providing oprerations for set size determination and for set difference (subtraction) defined as:

\begin{center}
$A \setminus B = \{ x | x \in A \land x \notin B \}$
\end{center}


For every graph, I generate a set of traces by representing each syscall as a string and subsequent calculating all paths from the root to some leaf -- producing a set of traces. 

 Having many graphs, I generate multiple sets of traces. Subsequently, I select the largest set and difference it with the rest of the sets. By repeating this approach, I attempt to cover the biggest amount of possible system call traces using the greedy approach. 


\begin{figure}[h!]
\begin{tikzpicture}[sibling distance=10em,
  level distance=6em,
  every node/.style = {shape=rectangle, rounded corners,
    draw, align=center,
    top color=greenl2!70, bottom color=greenl2!80, font=\small}]]
  \node {Start}
    child { node {Syscall:1\\ Thread:1\\ failed} }
    child { node {Syscall:2\\  Thread:1\\  passed}
      child { node{Syscall:52\\  Thread:2\\  passed}
        child { node{Syscall:56\\  Thread:1\\  passed} }
        child { node{Syscall:54\\  Thread:1\\  passed} }
        child { node{Syscall:1\\  Thread:2\\  passed} } }
      child { node{Syscall:12\\  Thread:1\\  failed} } };
\end{tikzpicture}
\caption{
        A simple representation of system calls as a tree
    }
  \label{fig:fuzzer::trie}
\end{figure}


