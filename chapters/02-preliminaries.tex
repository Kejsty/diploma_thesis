
\chapter{UNIX and File system} \label{chap:prelim}

In this thesis, I am working on and with system calls employing their syntax and semantics described by UNIX standard. Thereupon, I will introduce system calls including their syntax and semantics and file system with respect to UNIX operating system more precisely.


\section{UNIX}

\subsection{POSIX}

In the early years, there was an ambiguous task to develop a program that is highly portable across a variety of computer models, because each computer had a unique program architecture and a unique operating system and therefore it was not compatible with others. Programmers often had to rewrite their applications for every new computer model. 

In the 1960s, AT\&T's Bell Labs developed a new operating system AT\&T UNIX System capable of running across machines from multiple vendors. This system was subsequently forked as a base for other systems, such as Berkeley Standard Distribution UNIX, or Xenix. However, it was still challenging to port applications between these forked systems. 

In the 1980s, a POSIX standard was developed to resolve this portability issue. Based on System V and BDS Unix, it defined only the interface between an operating system and an application rather than the whole operating system.

In 1988, the Computer Society of IEEE \footnote{the world's largest technical professional society dedicated to advancing innovation and technological excellence} specified and published the IEEE Std 1003.1-1988, commonly known as POSIX or the IEEE Portable Operating System Interface for Computing Environments. This was an American National Standard which defined a standard way for an application program to obtain basic services from the operating system.  In 1990, IEEE Std 1003.1-1990 was published as an International Standard \cite{POSIXstandard}.

Nowadays, POSIX refers to a family of IEEE standards with the international standard name ISO/IEC 9945.

All POSIX-conforming systems must implement functions defined by POSIX, and programs that follow the POSIX standard use only these functions to obtain services from the operating system and the underlying hardware \cite{POSIXstandard}. These applications are then easier to move between various POSIX-conforming operating systems. 


\subsection{Compliant systems}

POSIX is a formal document written in very technical and precise form. It is written by and for system implementer and can be interpreted as a manual for writing a POSIX-conforming operating system.

To identify an operating system as a POSIX-certified, the system must have passed automated conformance tests \cite{IEEECertification}. Between POSIX-certified operating systems belongs, for example, macOS or Solaris. 

Additionally, numerous operating systems are not officially POSIX-certified but comply in large part. Here belong most distributions of Linux, Android or OpenBSD. 

There are also environments following POSIX standard for Microsoft Windows, among others Cygwin and MinGW.

\section{File System in UNIX}

A filesystem is a collection of data structures and methods that an operating system employs to keep track of the state of files on a disk.

More accurately, a filesystem is a hierarchical storage system for data in the form of files, directories, links, etc. which provides specific operations such as creation or deletion of files, moving between various directories, creating link and symlinks to files and mounting of filesystems ~\cite{LinuxKernelDev}. 
\newpage
\subsection{Inode}


The inode is an aggregation of information held by the kernel about every file or directory.  Every such inode object is uniquely identified by its address, often called a \textit{inode number}.

Note that two hard links pointing the same file have the same \textit{inode number}. Thus, from knowing the \textit{inode number} of two different paths in a directory, we can deduce whether these two paths point to the same file. This behaviour is demonstrated in  Fig.~\ref{fig:inodeExample}. Here I create two independent files \textit{file}, \textit{otherfile}, and one hard link, \textit{filelink}. In the final listing, one can notice that \textit{filelink} and \textit{file} have the same \textit{inode number} whereas the \textit{inode number} of \textit{otherfile} is clearly different. 

\begin{figure}[h!]
\begin{minted}{bash}
kejsty@kejsty:~/example$ touch file
kejsty@kejsty:~/example$ ln file filelink 
kejsty@kejsty:~/example$ touch otherfile
kejsty@kejsty:~/example$ ls -i
5489047 file        5489047 filelink    5489122 otherfile
\end{minted}
\caption{
      Demonstration of \textit{inode number} behaviour on files and hard links.
    }
  \label{fig:inodeExample}
\end{figure}



\section{System calls}


System calls, syscalls for short, are the fundamental interface between an application and the kernel~\cite{Man_syscall} and provide the primary interface for hardware access. Having this interface, the operating system can fully control access to its file system. 

Considering different hardware specifications, system calls also serve as an application programming interface (API) between the operating system and hardware. I focus on the POSIX API system calls, whose wrapper functions are part of the standard C library (\texttt{libc} in Unix systems) or specifically the the GNU C Library ~\cite{glibc} \- \texttt{glibc} in most Linux systems.


As already mentioned, one of the intentions behind system calls is that the program won't have direct access to the file system and the kernel. For these reasons, most CPUs differentiate between kernel and user mode. For a program to access hardware or kernel memory, it needs to switch into kernel mode. This switch is, however, possible only through a system call. \\

\subsection{File descriptor}

To be able to access some file or its data, we must first ask the operating system to open it. For these purposes, we can use system call \texttt{open} \cite{Man_open}.

The result of a successful open is a \textit{file descriptor} that is a handle through which the file can then be subsequently accessed \cite{unixFilesystem}. This file descriptor is needed in other system calls such as \texttt{write} or \texttt{read}, where it serves as a reference for an opened file, used by the operating system. 

\subsection{Stat}

To obtain the properties of any file type, we can use the system call \texttt{stat()} which takes a path to a file and returns information about this file. Eventually, we can use system call \texttt{fstat()} which works over already mentioned file descriptors instead of paths. 
\begin{figure}[h!]
\begin{minted}{bash}
kejsty@kejsty:~/example$ stat file.c 
  File: 'file.c'
  Size: 199             Blocks: 8          IO Block: 4096   regular file
Device: 801h/2049d      Inode: 5116012     Links: 1
Access: (0664/-rw-rw-r--)  Uid: ( 1000/  kejsty)   Gid: ( 1000/  kejsty)
Access: 2017-02-03 14:53:41.597784860 +0100
Modify: 2017-01-26 23:18:55.873924732 +0100
Change: 2017-01-26 23:18:55.873924732 +0100
Birth: -
\end{minted}
\caption{
      Regular file properties presented as output of invoking the \texttt{stat} utility on a regular file \texttt{file.c}. This utility internally uses the system call \texttt{stat()} and reproduces its output.
    }
  \label{fig:regFileProp}
\end{figure}


\begin{figure}[h!]
\begin{lstlisting}[style=C]
int stat(const char *pathname, struct stat *statbuf);

struct stat {
     dev_t     st_dev; 
     ino_t     st_ino; 
     mode_t    st_mode; 
     nlink_t   st_nlink; 
     uid_t     st_uid;    
     gid_t     st_gid;   
     dev_t     st_rdev; 
     off_t     st_size;      
     blksize_t st_blksize;  
     blkcnt_t  st_blocks; 

     /* Since Linux 2.6 */
     struct timespec st_atim;  
     struct timespec st_mtim; 
     struct timespec st_ctim; 
 };
\end{lstlisting}
\caption{
     The interface of \textit{stat} system call and the standard defined stat structure \cite{Man_stat}.
    }
  \label{fig:stat_syscall}
\end{figure}

Every regular file has several properties, as shown in Fig.~\ref{fig:regFileProp}, among others permissions or size of the file. To obtain this information for any file, there is a system call \textit{stat}. In a C program, its invocation is handled through the interface described in Fig. \ref{fig:stat_syscall}. 

The second attribute of this system call wrapper is a \textit{stat} structure defined below the syscall declaration. This structure holds various file properties. For instance, \texttt{st\_mode} provides the type of the file (regular, directory, socket, etc.) in the bit mask for the file type bit field of \texttt{st\_mode} and information about file’s permissions in the least significant 9 bits of \texttt{st\_mode}. The property \texttt{st\_size} provides information about the size of the file. Note that the \textit{inode number} is present as well in the \texttt{st\_ino} property. 

\subsection{ Errno } \label{chap:prelim:errno}

Most system calls return the value $-1$ in case of failure.  This rule does not hold universally, for example, the system call \texttt{getcwd} returns a pointer \cite{Man_getcwd} and in case of failure returns \texttt{NULL}. 

To differentiate between various causes of failure of a given call, the global variable \textit{errno} is filled with the cause of the error. The cause is represented by an \textit{error code} specified by POSIX.1-2001 and the C99 standard~\cite{Man_errno}. The program can access the variable \textit{errno} and interpret it as a reason for the failure.

A typical example is \texttt{EACCES} in case of insufficient permissions for manipulation with a file or \texttt{EBADF} in case the file descriptor provided to a system call is not valid. 

