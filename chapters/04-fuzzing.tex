\chapter{ Fuzzing } \label{chap:fuzzy}

Fuzzing is a highly automated testing technique that covers numerous boundary cases using possibly invalid data (from files, network protocols, API calls, and other targets) as application input to ensure the absence of exploitable vulnerabilities \cite{fuzzing4}. If we think of a system as a complex state machine, then fuzzing can be considered as a random walk searching for undefined and erroneous states.

A program that performs fuzzing of some program is often referred to as a \textit{fuzzer}.

\section{History}

The name fuzzing relates to a tendency of modem applications to fail upon receiving random input generated as a result of line noise on fuzzy telephone lines \cite{fuzzing1}. To be more accurate, the term \textit{fuzzing} was first formulated by Miller in 1988 during a class project, in which they examined crashes of UNIX utilities caused by a line noise over a \textit{fuzzy} modem connection \cite{perf_fuzzer}.

Miller et al. \cite{fuzzing3} subsequently extended this project with producing various causes of failures on a wide range of UNIX systems and finally investigated and generalised some frequent reasons causing a crash of a Unix system. They have found many bugs including an absence of array bounds checking and NULL pointer dereferences.

 \section{Types}
 
 As the definition of fuzzing is rather general, we can differentiate fuzzers based on their approach to fuzzing. 
 
 Since fuzzing is essentially a testing method, we can refer to a fuzzer as to a white-box, grey-box or black-box. This identification is adopted from testing and is based on the amount of information the fuzzer has about the structure of the tested program. Additionally, the fuzzer can be aware not only of the program structure itself but also of the input structure of the tested program.

Another differentiable category can be derived from the process of generating inputs for the tested program. The fuzzer can generate inputs by modifying preexisting data, for instance by using a biology-inspired technique such as mutation, crossover, and selection of inputs. Such approaches are known as genetic algorithms \cite{geneticAlgo}. 

We can look at fuzzing as on a component of negative testing, where unexpected or semi-valid inputs or sequences of inputs are sent to the tested program, instead of the proper data expected by the processing code. 


\subsection{Black-box}

 A black-box fuzzer in usually unaware of the structure of the program and thus has no particular knowledge of the internal operations of the underlying system. The system under test is stressed with unexpected inputs and data structures through an external interface. 
 
 Using only randomly generated inputs leads to finding mostly naive programming errors. However, there are attempts to incrementally learn about the internal structure of a system during its fuzzing which is done mainly through observing the program's output. 
 
 An example of such a system is LearnLib, a library for automata learning and experimentation \cite{LearnLib}. Given a list of messages it can send to the tested program --  known as the input alphabet -- LearnLib tries to come up with hypotheses for the state machine based on the responses it receives from the program using automaton learning. One of its possible application is TLS fuzzing \cite{TLSfuzzing}.

\subsection{White-box}
 
A white-box fuzzer is fully aware of program structure, including the source code of the program. Knowing the program source code allows the fuzzer to use a metric of its progress based on the code such as code coverage. Therefore, white-box fuzzer usually uses program analysis to increase code coverage systematically.

Another possible approach is to locate a critical location of a program such as an assert and attempts to reach it. That provides a sense of progress as well. 

Considering the fuzzer knows the source code of the program, it's naturally easier to locate even a deeply hidden bug. However, the time used for program analysis (or even its specification) can become expensive. Therefore,  even though white-box fuzzer may generate a considerably smaller amount of inputs until finding a bug, the time used for producing a corresponding input may be considerably longer.

An example of such a system is SAGE -- Scalable, Automated, Guided Execution, a whole-program
white-box fuzzing tool for x86 Windows applications, which records an actual run of the program under test on a well-formed input, symbolically evaluates the recorded trace and gathers constraints by watching program's behaviour on these traces \cite{SAGE}.

\subsection{Gray-box}

Finally, a gray-box fuzzer technique attempts to take advantage of both white- and black-box approaches. These fuzzers often operate with instrumentation rather than program analysis to collect information about the tested program and often exploit code coverage to measure their progress.

An example of such a system is AFL -- American fuzzy loop, a security-oriented fuzzer that employs a novel type of compile-time instrumentation and genetic algorithms \cite{AFL}. This system has a relatively large active community and has helped to detect various software bugs in many free software projects, including Qt, BIND, bash or OpenBSD \cite{AFLbugs}.

\section{Structure}

According to \cite{fuzzingBook}, there is a logical structure that should a typical modern fuzzer exhibits, described in Fig. \ref{fig:fuzzing:structure}.



\begin{figure} 
\includegraphics[scale=0.4]{img/generic_fuzzer_structure.png}
\caption{
        Generic structure of a fuzzer from \cite{fuzzingBook}.
    }
  \label{fig:fuzzing:structure}
\end{figure}

The \textit{Protocol model} holds models for diversified data formats or message sequences. 
The form of the model is not strictly defined; it can be described by simple message templates or more complex model based on e.g. context-free grammars. 

\textit{SUT analysis} is a runtime analysis engine responsible for monitoring, interacting with and controlling the system under test (SUT) and its environment.

\textit{Anomaly library } is an optional collection of inputs which often causes failures in the software. 
\textit{User Docs} is an optional test case documentation.

\textit{Attack simulation} is an engine that uses the anomaly library to learn from it and to generate some actual fuzzy tests to be run on the system.

Finally, \textit{reporting} is a preparation of test results in a human-readable form which gives conclusions about the system under test.


\section{Process}

Knowing the generic structure of a fuzzer, we can describe its workflow. 

The central part of fuzzing is naturally sending messages to the system and receiving signals from the system under test. An equally important part is the analysis of the received messages.

The result of a fuzzer should be a list of inputs causing errors in or even a crash of the system under test, or some other anomalous behaviour. There is no need to report valid inputs as there is an expectation that the majority of inputs will be valid. 

Every erroneous input produced by a fuzzer should be analysed individually by the user, as the input may not refer to an actual error. 


\section{Fuzzing of system calls} \label{chap:fuzzy_syscall}

As one part of this thesis in employing fuzzing technique on system calls traces, I reviewed already published methods used for fuzzing system calls.

The usual approach used when fuzzing a system call is mostly operating system oriented.
The idea is that the user provides various modified data obtained by fuzzing as input for system calls and monitor system for possible unexpected behaviour or crashes \cite{Trinity}, \cite{perf_fuzzer}.

\subsection{Trinity}

Trinity\cite{Trinity} is a broad spectral Linux System call fuzz tester. By broad spectral is meant that it focuses on an ability to fuzz every system call rather than targets one specific system call. 



To be aware of specific system call structure, every syscall has its arguments annotated, and where possible, it tries to provide something at least semi-sensible \cite{Trinity}. The type of argument can be \textit{Address} as a random memory address,  \textit{Pid} as random process id,  \textit{Len} as a random size of a variable or \textit{Fd} as an arbitrary file descriptor selected from a list of various file descriptors. 


The list contains multiple file descriptors to various file types because as already show in \textit{Chap.} \ref{chap:prelim}, many systems calls works with file descriptors. The list is obtained during starting Trinity by opening pipes, creating sockets with random network protocols and by scanning \texttt{sysfs, procfs}, and \texttt{/dev}. These file descriptors and additionally often shared between multiple processes.  Therefore, when some system call expects a file descriptor (interpreted as a \textit{Fd} annotation) it gets randomly one from the list. 


When it comes to generating random values, \textit{Trinity} prefers to pick values that are valid, close to correct, or generally known boundary and corner cases rather than always passing purely random values into the kernel. Primarily, for a string, it generates not only regular ASCII character sequence but also pathological cases with lots of nulls or weird Unicode value.

Trinity is quite popular fuzzer which has already triggered various bugs listen on their web page. 


\subsection{perf\_fuzzer}

Apart of the idea to fuzz all system call the \textit{perf\_fuzzer} explored the effectiveness of using specific domain knowledge and focused on finding bugs and security issues related to a single Linux system call \cite{perf_fuzzer} -- the \texttt{perf\_event\_open}. 


The \texttt{perf\_event\_open} call creates a file descriptor that allows measuring performance information \cite{Man_perf_event_open}. This design predetermines it to be used as the primary entry point into the Linux kernel’s \texttt{perf\_event} performance monitoring subsystem, which can be considered as a complex interface as it has grown to handle over 40 arguments that interact in subtle ways. The important feature of this system call is that its system-wide measurement generally requires root permissions. Thus this call can access more vulnerable data and cause further critical failures.

Focusing on one specific system call,  \textit{perf\_fuzzer} can find bugs that are undiscoverable by generic broad spectral system call fuzzers. Their approach is based on Trinity, however, instead of letting it run randomly and therefore calling  \texttt{perf\_event\_open} only occasionally, \textit{perf\_fuzzer}  targets that system calls, which use the  \texttt{perf\_event\_open} call and with random attributes from beforehand selected area, e.g. not entirely random. 
