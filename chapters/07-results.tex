\chapter{ Measurements } \label{chap:results}

In this chapter, I introduce the approach taken while measuring the difference between \texttt{select} and \texttt{fuzz} approaches.

\section{test programs}

I implemented two programs riddled with common programming bugs such as null pointer dereference, memory management errors or misguided handling of system call failure. Subsequently, I used the fuzzer and attempted to discover these bugs.

The program contains bugs divided into three categories by their origin.
\begin{itemize}
\item $f_x$ are errors caused by wrong \texttt{free} usage. Either double free or free of not-allocated memory.

\item $n_x$ are errors caused by dereferencing \texttt{NULL}.

\item $o_x$ is an off-by-one problem.
\end{itemize}

However, as we are fuzzing traces, we are not interested in the origin of the fault but rather in the path taken to cause the error. It may be interesting to see which defects can be easily found by fuzzing traces and which require a specific arrangement of syscalls in a trace.

\subsection{Program 1}

The first tested program tried to find the list of authors on the \divine web page. Upon starting, the program creates a socket, connects the socket to the \divine web server, obtains the data and parses them to find the authors. Retrieved authors are stored in a custom vector implementation. The program is written in the C programming language to show communication using only system calls in the most elementary way.

This program is not parallel, but due to its communication through sockets, it cannot be verified merely by \divine. \\

Bugs which occur in this program are:
\begin{itemize}
\item $f_1$ can be reached only if the system call \texttt{connect} returns $-1$. That is a fairly easy condition but can be obtained only when the system call fails.

\item $f_2$ can be triggered if no author occurs in a trace (there are two authors in the original trace).
\item $o_1$ can be produced only if more than two authors occur in a trace (there are two authors in the original trace).

\item $n_1, n_2$ can be triggered if a search for a substring ends up with no match found. This state is quite easy to reach by changing the right bit or character. However, these bugs are nested, meaning that to reach $n_2$, $n_1$ must not fail. That makes $n_2$ quite hard to reach.
\end{itemize}

The program causes approximately $15$ system calls of which $10$ can be modified, not counting modification of return values. With \divine version $4.1$, the program in \texttt{verify} mode on the original \texttt{passthrough.out} trace generates $2547$ states with $1546$ transition and uses $731264$ bits of memory.



\subsection{Program 2}

The second tested program is a program simulating a Battleship game, a guessing game for two players, based on locating opponent's fleets. There are two threads, one is an attacker and the second is a defender. They communicate through two pipes. Upon launching, the defender initialises a board of ships and expects attacks. The attacker, having a list of attacks, starts to attack by sending a position from the attack list to the defender. The program is also written in C.

The program causes $191$ system calls of which $155$ can be modified, not counting modification of return value. This program is parallel, generates $2869$ states with $8526$ transitions and uses $749424$ bits of memory.

The bugs occurring in this program are:
\begin{itemize}

\item $o_1, o_2$ can be found if an attack string is not in the expected format [A-I][$1-9$]; however, the first character must still be alphabetic and the second must always be a number.
\item $o_3$ happens if a result of the attack is successful more than four times in a row (this is because a ship has size at most $3$; thus the fourth attack is expected to be unsuccessful).
\item $f_1$ occurs if the defender receives info about unexpected termination of the attacker. This system call does not occur in default trace thus may only develop as a modification of some existing syscall in the trace.

\end{itemize}



\section{Discussion of results}

I have made several comparisons for both \texttt{fuzz} and \texttt{select} techniques. For both programs, I have primarily done measurement for all combinations of \texttt{fuzz} and \texttt{select} techniques for a limited running time. This means that for every such a combination, I ran the fuzzer for a limited time and after it stopped, I counted the number of occurrences of each bug in graphs produced by \divine (using the fuzzed traces).

From these results, I have selected one particular \texttt{select} approach and one particular \texttt{fuzz} approach. I have evaluated the progression of the chosen \texttt{select} approach with every \texttt{fuzz} approach and with the running time gradually increasing.
Similarly, I have taken one particular \texttt{fuzz} approach and compared its effectiveness with each \texttt{select} technique with the running time increasing. Note that even though the running time increases, these measurements are independent.

Since the user may be interested in the first occurrence of a bug rather than in the frequency of finding the bug, I also included results showing the first graph in which each bug occurred (if it was even found). Note, that the graphs are produced in ascending order; thus the graph's number reasonably approximates time needed to discover this bug. 


\newpage

\begin{table}[ht]
 \centering
\tabcolsep=0.18cm
\begin{tabular}{|p|p|ccccc|}
\hline
\rowcolor{greenf!70}

Select & Fuzz & $f_1$ & $f_2$ &  $n_1$ & $n_2$ & $o_1$  \\

& bits & 0 & 107 & 223 & 0 & 104 \\
& random & 0 & 0 & 10 & 0 & 43 \\
& string & 0 & 8 & 37 & 1 & 113 \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{All}} & return & 14 & 6 & 26 & 1 & 13 \\
\hline
& bits & 0 & 28 & 236 & 0 & 12 \\
& random & 0 & 1 & 36 & 0 & 8 \\
& string & 0 & 9 & 153 & 1 & 16 \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Longest}} & return & 33 & 10 & 108 & 0 & 11 \\
\hline
& bits & 0 & 36 & 54 & 0 & 33 \\
& random & 0 & 1 & 5 & 0 & 20 \\
& string & 0 & 18 & 49 & 1 & 279 \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Fanout}} & return & 27 & 11 & 37 & 2 & 31 \\
\hline
& bits & 0 & 765 & 155 & 0 & 1 \\
& random & 0 & 173 & 13 & 0 & 4 \\
& string & 0 & 0 & 526 & 0 & 6 \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Coverage}} & return & 42 & 176 & 584 & 0 & 6 \\

\hline
\end{tabular}
\caption{Results of fuzzer on program $1$, with every \texttt{fuzz} option and every \texttt{select} option, with restricted running time $2$ hours. Columns represent the number of occurrences of a bug in all graphs produced during the limited run of fuzzer. }
\label{tab:results::p1::all}
 \end{table}


\subsection{Program 1}

The measurements for all combinations can be found in \textit{Tab.} \ref{tab:results::p1::all}.

As expected, the bug $f_1$ was discovered only when the tested \texttt{fuzz} approach modified the return values and \texttt{errno}.
It also looks like the rather primitive \texttt{fuzz} approaches \textit{bits} and \textit{random} may be too ineffective to generate a trace that discovers error $n_2$ in a reasonable time.

Regarding the \texttt{select} approach, there seems to be a relatively small difference between various selection techniques in case of this program. The reason for that may be that this program is sequential.

\newpage

\begin{table}[ht]
 \centering
\tabcolsep=0.18cm
\begin{tabular}{|p|p|ccccc|}
\hline
\rowcolor{greenf!70}

Select & Fuzz & $f_1$ & $f_2$ &  $n_1$ & $n_2$ & $o_1$  \\

& bits & $\times$ & 3 & 14 & $\times$ & 68 \\
& random & $\times$ & $\times$ & 17 & $\times$ & 12 \\
& string & $\times$ & 216 & 60 & 703 & 5 \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{All}} & return & 176 & 131 & 47 & 789 & 26 \\
\hline
& bits & $\times$ & 69 & 18 & $\times$ & 53 \\
& random & $\times$ & 157 & 169 & $\times$ & 25 \\
& string & $\times$ & 21 & 14 & 80 & 64 \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Longest}} & return & 23 & 1 & 16 & $\times$ & 13 \\
\hline
& bits & $\times$ & 11 & 18 & $\times$ & 4 \\
& random & $\times$ & 468 & 39 & $\times$ & 144 \\
& string & $\times$ & 13 & 30 & 543 & 1 \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Fanout}} & return & 7 & 8 & 23 & 81 & 85 \\
\hline
& bits & $\times$ & 13 & 78 & $\times$ & 17 \\
& random & $\times$ & 133 & 129 & $\times$ & 9 \\
& string & $\times$ & $\times$ & 1 & $\times$ & 32 \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Coverage}} & return & 7 & 43 & 77 & $\times$ & 52 \\

\hline
\end{tabular}
\caption{Results of fuzzer on program $1$, with every \texttt{fuzz} option and every \texttt{select} option, with restricted running time $2$ hours. Each column represents the first graph (with respect to time) in with the bug occurred, or $\times$ if it was not found during the time limit $2$. }
\label{tab:results::p1::first}
 \end{table}

 The results of first occurence of a bug in graphs produced by the fuzzer in the limited running time $2$ hours are listed in \textit{Tab.} \ref{tab:results::p1::first}. This comparison indicated, that the \texttt{fuzz} technique \textit{bit} is quite powerfull in finding bugs $f_2$, $n_1$ and $o_3$, whereas \textit{random} approach seems to have problems with discovering most of the bugs, apart from $o_3$. The results of \textit{return} technique combined with all \textit{fanout}, \textit{coverage}, \textit{longest} and even \textit{all} approaches seems to have quite satisfying results in finding bugs in this program.



\newpage

 \begin{table}[ht]
 \tabcolsep=0.10cm
 \begin{tabular}{|p| c c c c|}
 \hline
 \rowcolor{greenf!70}
 \backslashbox{Bug}{Select} & All & Coverage & Longest  & Fanout \\ [0.5ex]
 \hline
 $f_1$ & 15/28/52 & 14/34/65      & 17/27/73   & 11/32/741 \\
 \hline
 $f_2$ & 9/21/51 & 193/215/329    & 8/1/10     & 4/17/13 \\
 \hline
 $ n_1$ & 5/14/84 & 125/656/1.5k  & 38/122/312 & 15/30/41 \\
 \hline
 $ n_2$ & 0/0/0   & 1/0/0         & 0/1/0      & 0/1/0 \\
 \hline
$ o_1$ & 6/7/46   & 8/4/8         & 12/16/18   & 9/24/32 \\
 \hline
\end{tabular}
\caption{esults of fuzzer on program $1$, with every \texttt{select} options and fixed \texttt{fuzz} option(\textbf{return}), with limited  runing time limits. Row holds number of occurences of a bug. Each cell contains frequency listed in format $1$ hour/$2$ hours/ $4$ hours.R }
\label{tab:results:p1:fuzzy}

\end{table}



To compare different \texttt{select} approaches for this program, I decided to take the \texttt{fuzz} technique \textit{return}, as this technique is the only one capable of discovering $f_1$.
With this technique fixed for every run of the fuzzer, I evaluated its ability to find bugs with gradually increasing running times ($1$ hour, $2$ hours, $4$ hours). The results are listed in \textit{Tab.} \ref{tab:results:p1:fuzzy}.

The results indicate that finding bug $n_2$ in the first program seems to be a coincidence rather than a result of a good \texttt{select} technique.
Also, the discovery rate of some bugs seems to increase with increasing time, such as $f_1, n_1$, but the frequency of others appears to non-linear in some cases.
Therefore, I would label bugs $f_1, n_1$ as stable and rather easily discoverable.

There is interesting non-linearity in the case of the \textit{coverage} approach and bug $o_1$. Considering also the frequency of finding this bug, it may indicate that the \textit{coverage} may have a bigger problem to discover this bug than for example \textit{longest}.

There is also obvious non-linearity in the case of the \textit{longest} approach and bug $f_2$. If we consider the small frequency of finding this bug, compared to for example \textit{coverage}, we may again reason that this technique has a problem discovering this bug. 

Another interesting result to point out in \textit{Tab.} \ref{tab:results:p1:fuzzy} is the high frequency of finding bug $n_1$ with the \textit{coverage} technique. This may indicate that this technique became stuck after discovering this bug by re-discovering in over and over again. 


\newpage

\begin{table}[ht]
 \begin{tabular}{|p|c c c c|}
 \hline
  \rowcolor{greenf!70}
\backslashbox{Bug}{Fuzz} & Bits & Random & String  & Return \\ [0.5ex]
 \hline
 $f_1$ & 0/0/0       & 0/0/0       &  0/0/0      & 14/34/65 \\
 \hline
 $f_2$ & 46/288/476  & 1/0/0       & 34/5/11       & 193/215/329 \\
 \hline
 $ n_1$ & 234/440/870 & 16/28/507   & 166/138/205  & 125/656/1.5k \\
 \hline
 $ n_2$ & 0/1/0      & 0/0/1       & 0/1/1        & 1/0/0 \\
  \hline
  $ o_1$ & 5/4/11    & 5/8/17       & 2/126/10       & 8/4/8 \\
 \hline
\end{tabular}
\caption{Results of fuzzer on program $1$, with every \texttt{fuzz} options and fixed \texttt{select} option(\textbf{coverage}), with limited  runing time limits. Row holds number of occurences of a bug. Each cell contains the frequency listed in format $1$ hour/$2$ hours/ $4$ hours. }
\label{tab:results:p1:select}
\end{table}
To compare different \texttt{fuzz} approaches for this program, I decided to take the \texttt{select} technique with the highest sum of discovered bugs according to \textit{Tab.} \ref{tab:results::p1::all} (\textit{coverage}),
 and evaluate its ability to find bugs with gradually increasing running times ($1$ hour, $2$ hours, $4$ hours). The results are listed in \textit{Tab.} \ref{tab:results:p1:select}. 

These results support the hypothesis that discovering $n_2$ seems to be a coincidence. The technique \textit{random} seems to have problems discovering bugs $f_1$ and $f_2$. The high frequency of discovering $n_1$ seems to be a property of the \texttt{select} approach(\textit{coverage}) rather than the outcome of some \texttt{fuzz} technique. 

 Overall, the results indicate that the most suitable \texttt{fuzz} option for this particular program is \textit{return}, as it managed to discover the bug $f_1$ (as the only one), and it was also able to trigger the rest of the bugs eventually. 



\subsection{Program 2}

Program 2 has noticeably more system calls than program 1, and therefore, there is an expectation that the bugs will be triggered with more significant difficulty. The main reason is that the probability of fuzzing the right system call on the best place lowers with the increasing number of syscalls. For these purposes, I ran the fuzzer for four hours to collect results for all combinations. The results can be seen in \textit{Tab.} \ref{tab:results::p2::all}. A few observations are worth pointing out.

\newpage

 \begin{table} [ht]
 \centering
\tabcolsep=0.18cm
\begin{tabular}{|p|p|cccc|}
\hline
\rowcolor{greenf!70}

Select & Fuzz & $o_1$ & $o_2$ &  $o_3$ & $f_1$  \\

& bits & 0 & 0 & 46 & 0  \\
& random & 0 & 0 & 0 & 0  \\
& string & 0 & 22 & 100 & 0  \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{All}} & return & 0 & 0 & 0 & 0  \\
\hline
& bits & 0 & 0 & 84 & 0 \\
& random & 0 & 0 & 2 & 0 \\
& string & 0 & 16 & 44 & 0 \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Longest}} & return & 0 & 0 & 0 & 0  \\
\hline
& bits & 0 & 0 & 4 & 0 \\
& random & 0 & 0 & 0 & 0 \\
& string & 0 & 18 & 616 & 0 \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Fanout}} & return & 0 & 0 & 0 & 0 \\
\hline
& bits & 0 & 0 & 92 & 0 \\
& random & 0 & 0 & 0 & 0 \\
& string & 0 & 34 & 40 & 0 \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Coverage}} & return & 0 & 2 & 0 & 10 \\

\hline
\end{tabular}
\caption{Results of fuzzer on program $2$, with every \texttt{fuzz} option and every \texttt{select} option, with restricted running time $4$hours. Columns represent the number of occurrences of a bug in all graphs produced during the limited run of fuzzer. }
\label{tab:results::p2::all}
 \end{table}



First, the \texttt{select} technique \textit{random} seems to be unable to detect any bugs occurring in this program.
The \textit{return} technique appears to be the only one that discovered the bug $f_1$, but it seems incapable to trigger the remaining bugs. This may be because fuzzing of return values caused earlier termination of the program -- this naturally decreases the probability of finding the bugs, as only a few syscalls from the generated trace are executed.

Secondly, the bug $o_1$ seems hard to discover; however, according to \textit{Tab.} \ref{tab:results:p2:fuzzy}, it can be triggered when running fuzzer for a longer time -- probably because it requires a more specific scenario.


\newpage

\begin{table} [ht]
\centering
\tabcolsep=0.18cm
\begin{tabular}{|p|p|cccc|}
\hline
\rowcolor{greenf!70}

Select & Fuzz & $o_1$ & $o_2$ &  $o_3$ & $f_1$  \\

& bits & $\times$ & $\times$ & 5 & $\times$  \\
& random & $\times$ & $\times$ & $\times$ & $\times$  \\
& string & $\times$ & 81 & 6 & $\times$  \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{All}} & return & $\times$ & $\times$ & $\times$ & $\times$  \\
\hline
& bits & $\times$ & $\times$ & 19 & $\times$ \\
& random & $\times$ & $\times$ & 242 & $\times$ \\
& string & $\times$ & 44 & 68 & $\times$ \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Longest}} & return & $\times$ & $\times$ & $\times$ & $\times$  \\
\hline
& bits & $\times$ & $\times$ & 8 & $\times$ \\
& random & $\times$ & $\times$ & $\times$ & $\times$ \\
& string & $\times$ & 43 & 1 & $\times$ \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Fanout}} & return & $\times$ & $\times$ & $\times$ & $\times$ \\
\hline
& bits & $\times$ & $\times$ & 27 & $\times$ \\
& random & $\times$ & $\times$ & $\times$ & $\times$ \\
& string & $\times$ & 40 & 10 & $\times$ \\
\multirow{-4}{*}{\rotatebox[origin=c]{90}{Coverage}} & return & $\times$ & 80 & $\times$ & 345 \\

\hline
\end{tabular}
\caption{ Results of fuzzer on program $2$, with every \texttt{fuzz} option and every \texttt{select} option, with restricted running time $4$ hours. Each column represents the first graph (with respect to time) in which the bug occurred, or $\times$ if it was not found during the time limit $4$ hours. }
\label{tab:results::p2::first}
\end{table}


The results of the first occurrence of a bug in graphs produced by the fuzzer with the limited running time $4$ hours are listed in \textit{Tab.} \ref{tab:results::p2::first}. The results reveals, that the both \texttt{fuzz} techniques \textit{bit} and \textit{string} has no particular problem with discovering bug $o_3$ in a quite short time. 
However, the \textit{string} technique is assitionally capable of discovering bug $o_2$. 
The results also indicate that finding the bug $f_1$ have indeed taken quite a long time. 

\newpage


\begin{table}[ht]
 \begin{tabular}{|p| c c c c|}
 \hline
 \rowcolor{greenf!70}
 \backslashbox{Bug}{Select} & All & Coverage & Longest  & Fanout \\ [0.5ex]
 \hline
 $o_1$ & 0/0/2 & 0/0/8 & 0/0/0 & 0/0/0 \\
 \hline
 $o_2$ & 22/136/34 & 14/26/32 & 2/24/60 & 12/26/16 \\
 \hline
 $ o_3$ & 2/6/10 & 2/2/14 & 10/48/52 & 0/8/2 \\
 \hline
 $ f_1$ & 0/0/0 & 0/0/0 & 0/0/0 & 0/0/0 \\
 \hline
\end{tabular}
\caption{Results of fuzzer on program $2$, with every \texttt{select} options and fixed \texttt{fuzz} option(\textbf{string}), with limited runing time. Row holds number of occurences of a bug. Each cell contains frequency of the bug listed in format $2$ hour/$4$ hours/ $8$ hours.}
\label{tab:results:p2:fuzzy}

 \begin{tabular}{|p|c c c c|}
 \hline
  \rowcolor{greenf!70}
\backslashbox{Bug}{Fuzz} & Bits & Random & String  & Return \\ [0.5ex]
 \hline
 $o_1$ & 0/0/0 & 0/0/0  & 0/0/8 & 0/0/0 \\
 \hline
 $o_2$ & 0/0/0 & 0/0/0  & 14/26/32 & 0/2/2 \\
 \hline
 $ o_3$ & 24/18/72 &  0/0/0  & 2/2/14 & 0/0/0 \\
 \hline
 $ f_1$ & 0/0/0 & 0/0/0 & 0/0/0 & 0/10/20 \\
 \hline
\end{tabular}
\caption{Results of fuzzer on program $2$, with every \texttt{fuzz} options and fixed \texttt{select} option(\textbf{coverage}), with limited runing times ($2$, $4$ and $8$ hours). Row holds number of occurences of a bug. Each cell contains frequency of the bug listed in format $2$ hour/$4$ hours/ $8$ hours }
\label{tab:results:p2:select}
\end{table}


For the subsequent measurements, I have chosen the \texttt{fuzz} technique \textit{string}, since it has found the widest number of bugs and performed quite stable (compared to \textit{random}), and the \texttt{select} technique \textit{coverage}, since it was able to trigged the widest range of bugs ($3$ out of $4$) according to \textit{Tab.} \ref{tab:results::p2::all}.

The results of comparison of all \texttt{select} techniques with fixed \texttt{fuzz} technique (\textit{string}) are listed in \textit{Tab.} \ref{tab:results:p2:fuzzy}.

Notice that while the \textit{all} \texttt{select} technique led eventually to the discovery of the bug $o_1$,
neither of the techniques \textit{fanout} and \textit{longest} have managed to accomplish that. This may be because the trace that produced this bug had been generated as a mutation of a trace that is neither the longest nor has the biggest fanout. This may, in turn, indicate that these \texttt{select} techniques are not suitable for this program.

The results of comparison of all \texttt{fuzz} techniques with fixed \texttt{select} technique (\textit{coverage}) are listed in \textit{Tab.} \ref{tab:results:p2:select}.

As already mentioned, the \texttt{fuzz} techniqe \textit{return} trigerred the bug $f_1$ as the only one, but had problems with other bugs. The \textit{random} approach is ineffective even with longer execution time, while the \textit{string} technique has managed to discover bug $o_1$ when running for 8 hours. The \textit{bits} technique still managed to trigger only bug $o_3$.


% Generally, the results indicate, that the best \texttt{fuzz} option for this particular program is \textit{string}, as it can as the only one discover the bug $f_1$, and it was able to discover all other bugs eventually. 





\section{Overall results}

Generally, there is no universal \texttt{fuzz} technique. For some programs, such as the first, sequential program, the best choice has been the \textit{return} approach. On the other hand, for the second, parallel program, the \textit{return} technique seemed to kill most of the runs with no bug produced, even though it triggered one bug as the only technique. Even though, in the case of the second program, the \textit{string} \texttt{fuzz} method seemed like a better choice. Techniques \textit{bits} and \textit{random} were always dominated by some other technique.

Looking at all \texttt{select} techniques, there seems to be no significant difference among them in case of the sequential program leaving out the \textit{all} approach. 
On the other hand, in the case of the parallel program, the \textit{coverage} approach seems like the best strategy. Even though it sometimes has not found the bugs with the most significant frequency, it managed to discover all the bugs of the second program eventually.
Additionally, as there happened to be cases where the \textit{all} technique performed better than some other, I would suggest enhancing the \texttt{select} approaches by also choosing a random trace occasionally for the next iteration.


Generally, I would suggest using both \textit{return} and \textit{string} \texttt{fuzz} techniques while validating some program using \divine and fuzzing. Both have different strengths. While some programs may rely on system calls succeeding, others may depend on the content of the received data.

Regarding the \texttt{select} technique, I would suggest using the \textit{coverage} technique, as it appeared to be the best strategy in this measurement -- it was the widest reaching technique.

