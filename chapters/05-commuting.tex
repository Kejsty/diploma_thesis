\chapter{ Commuting system calls } \label{chap:commuting}

In this chapter, I present my first approach taken to enlarge the coverage of the program verified using \divine. It is based on \textit{commuting system calls with respect to inodes}.

The introduction of passthrough and replay modes in \divine \cite{Kejstova2017thesis} created an opportunity to capture all system calls executed by a program under test including their inputs and outputs. Subsequently,  this captured trace was used to replay these system calls in verification mode. The replay mode was based on simulating system calls requested by the program using the already captured trace. This approach brought a possibility of verification with limited thread interleaving even for programs that communicate with the outer world, including programs communicating through a network using sockets.

However, verification was possible only with limited thread interleaving, in the sense that during replay, all system calls must have been executed in precisely the same order as during their capture. \divine could, therefore, verify all the execution paths of a system which respects the order of system calls in the captured trace. This condition is overly strict and can be refined.

\section{Commuting with respect to Inodes}

\begin{figure}[h!]
\begin{lstlisting}[style=C]
void *worker( void *file ) {
    char* data = (char*) file;
    printf("Opening: %s\n", data );
    int fd = open( file, O_RDWR );
    if( fd < 0 ){
        printf("Unable to open: %s\n", data );
        return NULL;
    }

    char incoming;
    while (read( fd, &incoming, 1 ) != 0) {
        if ( incoming == 'x' )
            write(fd, data, 1 );
    }
    return NULL;
}

int main() {
    
    pthread_t thread_a;
    pthread_t thread_b;
    pthread_create( &thread_a, NULL, worker, "a.txt" );
    pthread_create( &thread_b, NULL, worker, "b.txt" );

    pthread_join( thread_a, NULL );
    pthread_join( thread_b, NULL );
    return 0;
}
\end{lstlisting}
\caption{
      An example of a program where the result depends on whether \texttt{a.txt} and \texttt{ b.txt} are the same files or not. 
    }
  \label{fig:inode_rw_example}
\end{figure}


Consider a C program example from Fig. \ref{fig:inode_rw_example}. The program scans files \texttt{a.txt} and \texttt{b.txt} in parallel and upon finding an occurrence of character \texttt{x} writes \texttt{a} or \texttt{b} after the \texttt{x}. 

The usual result would be that \texttt{a.txt} would contain sequences of \texttt{xa} and  \texttt{b.txt} sequences of \texttt{xb}. In this case, we do not care whether some write of thread A will be executed before some write of thread B and vice versa.

The result of this program differs if \texttt{a.txt} and \texttt{b.txt} point to the same file within the file system. Here, the result of the program is non-deterministic, as every character \texttt{x} may be followed either by character \texttt{a} or character \texttt{b}.

Moreover, in the general case, we are unable to predict the behaviour when switching the order of execution of thread \textsf{A} and \textsf{B}. It may not affect the result, or it could replace some occurrences of \texttt{xa} with \texttt{xb} and vice versa.

Considering the replay of this program with \divine using a captured system call trace, we are unable to decide whether a write of thread \textsf{A} can be executed before a write of thread \textsf{B} as the behaviour of this action is unpredictable. \\
 
 
Therefore, we say that two system call \textit{commute with respect to inodes} if each works with a file with distinct \textit{inode number}. 

It would be incorrect to say, however, that two system calls that commute with respect to an \textit{inode} also commute in general. 



There is a somewhat artificial example of a watcher, running outside of a verified program, which performs an action based on which thread of the program executes a command as first. 

Imagine an outside watcher \texttt{W} looking at two files, $f_a and f_b$. If the content of $f_a$ changes, the watcher rewrites $f_b$ and the other way around. Now consider a program, launching two threads \texttt{A,B} simultaneously, where \texttt{A} works with $f_a$, and \texttt{B} with $f_b$. Clearly, the result of this program still depends on the order of \texttt{A,B}, even though $f_a, f_b$ are different files -- and thus have different \textit{inodes}.

As already shown, when two system calls work with the same \textit{inode} they usually fail to commute. What about system calls which do not work with files? We can differentiate them into two groups. There are system calls that are thread-dependent, meaning that they affect the behaviour of all running threads, and system calls that are truly independent.

The only fully independent system call supported by \divine is \texttt{uname}. This system call can commute at any time.

Example of thread-dependent system calls are \texttt{chdir} or \texttt{getcwd}, because changing a directory of one thread automatically changes the directory of all others threads of a given process.

\section{Implementation}


My implementation attempts to achieve the behaviour described above.

In case of system calls working with files that produce a \textit{file descriptor}, such as \texttt{open} or \texttt{socket}, I obtain the \textit{inode} of the opened file from its file descriptor, using system call \texttt{fstat}, mentioned in \ref{chap:prelim}. Subsequently, I remember the mapping between the \textit{file descriptor} and obtained \textit{inode}.

In case of system calls working with files, that do not produce \textit{file descriptor}, such as \texttt{chmod} or \texttt{mkdir}, I obtain the \textit{inode} using system call \texttt{stat}, mentioned in \ref{chap:prelim}.

In case of system calls working with \textit{file descriptors}, I benefit from the mapping and obtain the \textit{inode}.

Note, that I prefer to use one call to \texttt{fstat} while opening a file and then subsequent internal mapping, rather than a repetitive \texttt{fstat}. This is because of the possibility of \textit{file descriptor} invalidation.

 System call \texttt{close} invalidates the \textit{file descriptor}, therefore \texttt{fstat} could fail in some specific cases, such as double close of a file, or reading from a file after closing. In these cases, I could not obtain the \textit{inode} using \texttt{fstat}, even though the system call should be bounded to some \textit{inode}.


Handling of fully independent system calls is quite straightforward since they can be called at any time.

In the case of thread dependent system calls, I preserved the original implementation and thus allowed no commuting. 

\section{Resulting structure}

The resulting structure, that is created inside \divine during replay mode can then look like in \textit{Fig.} \ref{fig:replay_example}. 

Every \textit{inode} has its connected list of system calls in the order they appear in the captured trace.  Thread-dependent system calls also constitute an independent list in the same order. Independent system calls forms a multiset to ensure that when binding some system call it is always available.  

When a system call record from this structure is matched to a system call request from the program under test, the record is removed.

\begin{figure}[ht]
\begin{tikzpicture}[
  level 1/.style={sibling distance=30mm},
  edge from parent/.style={->,draw},
   basic/.style  = {draw, text width=2cm, drop shadow, font=\sffamily, rectangle},
  root/.style   = {basic, rounded corners=2pt, thin, align=center,
                   fill=greenl1!80},
  level 2/.style = {basic, rounded corners=6pt, thin,align=center, fill=greenl2!80,
                   text width=6em},
  level 3/.style = {basic, thin, align=left, fill=greenl3!80, text width=6.5em},
  >=latex]
    % \shorthandoff{-}

% root of the initial tree, level 1
\node[root] {Syscalls}
% The first level, as children of the initial tree
  child {node[level 2] (c1) {Inode 1768}}
  child {node[level 2] (c2) {Inode 8739}}
  child {node[level 2] (c3) {Independent}}
  child {node[level 2] (c4) {Thread-dependent}};

% The second level, relatively positioned nodes
\begin{scope}[every node/.style={level 3}]
\node [below of = c1, xshift=15pt] (c10) {\footnotesize Open(a.txt), 12};
\node [below of = c10] (c11) { \footnotesize Write(12,Hello),5};
\node [below of = c11] (c12) {\footnotesize Close(12), 0};
\node [below of = c12] (c13) {\footnotesize Open(b.txt), 14};
\node [below of = c13] (c14) { \footnotesize Write(14,Hello),5};
\node [below of = c14] (c15) { \footnotesize Close(14), 0};

\node [below of = c2, xshift=15pt] (c21) {\footnotesize Open(c.txt),17};
\node [below of = c21] (c22) {\footnotesize Fstat(17, info),0};
\node [below of = c22] (c23) {\footnotesize Stat(c.txt, info),0};
\node [below of = c23] (c24) {\footnotesize Read(17,data,3),2};

\node [below of = c3, xshift=15pt, yshift=-20pt] (c31) {MULTISET: \\
\footnotesize [Uname(info),0], \\
\footnotesize [Uname(info),0]}; \\

\node [below of = c4, xshift=15pt] (c41) {\footnotesize Getcwd(info),info};
\node [below of = c41] (c42) {\footnotesize Chdir(/tmp),0};
\node [below of = c42] (c43) {\footnotesize Getcwd(info),info};

\end{scope}

% lines from each level 1 node to every one of its "children"
\foreach \value in {0,...,5}
  \draw[->] (c1.195) |- (c1\value.west);

\foreach \value in {1,...,4}
  \draw[->] (c2.195) |- (c2\value.west);

\draw[->] (c3.197) |- (c31.west);
  
\foreach \value in {1,...,3}
  \draw[->] (c4.209) |- (c4\value.west);
\end{tikzpicture}


\caption{
      An example of internal structure for system calls held by \divine during replay mode. 
    }
  \label{fig:replay_example}
\end{figure}


\section{Results}




Figure Fig. \ref{fig:commutin_inode_result} shows how can commutation with respect to \textit{inodes} extend the state space covered and verified by \divine. The figure shows two state space graphs with different system call commutation strategies on a program \ref{fig:commuting_program} with 12 system calls working with two different \textit{inodes}.

The graphs in PDF form and the program's source code can be found in the attachment of this thesis. The guidebook to obtain this graph can be found in Appendix \ref{chap:appendixa}. More of such results can be found in the attachment of this thesis, always as a triple: program, graph with and without commuting.

There is a guarantee that the state space obtained by \textit{inode} commutation will be an extension of the originally produced graph, as the original state space appears as a subgraph in the state space obtained by \textit{inode} commutation.

The reason is that every path reachable by the original strategy is still reachable with \textit{inode} commutation.


\begin{figure}[h!]
\begin{lstlisting}[style=C]
void *worker( void *data ) {
    
    char* filename = (char*)data;
    int fd = open( filename, O_WRONLY | O_CREAT, 0666);

    write(fd, filename, strlen(filename));
    int res = close(fd);

    int fd2 = open(filename, O_RDONLY | O_CREAT, 0644 );
    char buff[ 15 ] = {};

    int readed = read(fd2, buff, strlen(filename));
    close( fd2 );

    assert( readed == strlen(filename));
    assert( strcmp( buff, filename ) == 0 );
    return NULL;
}

int main() {

    pthread_t thread_a;
    pthread_create( &thread_a, NULL , worker, "a_file" );
    pthread_t thread_b;
    pthread_create( &thread_b, NULL , worker, "b_file" );

    pthread_join( thread_a, NULL );
    pthread_join( thread_b, NULL );
    return 0;
}

\end{lstlisting}
\caption{
      A simplified version program used for verification in \texttt{replay} configuration both with and without commuting. Includes and checks for syscall success were removed for better readability. 
    }
  \label{fig:commuting_program}
\end{figure}


\begin{figure}
\centering
\begin{minipage}{.28\textwidth}
  \centering
  \includegraphics[width=\linewidth]{img/original_replay.png}
%   \subcaption{
%   \begin{footnotesize}
% (a) The original strategy.
% \end{footnotesize}
% }
  \label{fig:sub:original}
\end{minipage}%
\begin{minipage}{.72\textwidth}
  \centering
  \includegraphics[width=\linewidth]{img/inode_commuting.png}
%   \subcaption{
%     \begin{footnotesize}
% (b) The commuting strategy with respect to \textit{inode}
% \end{footnotesize}
% }
  \label{fig:sub:commuting}
\end{minipage}
\caption{Two state graphs produced over the program from \textit{Fig}. \ref{fig:commuting_program} with different system call strategies for replaying system calls during verify mode of \divine. }
\label{fig:commutin_inode_result}
\end{figure}
