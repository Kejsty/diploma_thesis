\chapter{ \divine and its Environment } \label{chap:divine}

% \section{\divine}

“\divine is a modern, explicit-state, LLVM-based LTL model checker that follows the standard automata-based approach to explicit-state model checking” ~\cite{Divine2}. Based on the LLVM toolchain, it can verify programs written in multiple real-world programming languages, including C and C++~\cite{Divine}. 

\begin{figure}[h!]
% \resizebox{\textwidth}{}{
\begin{tikzpicture}[ ->, >=stealth', shorten >=1pt, auto, node distance=2cm
                   , semithick
                   , style={ node distance = 1.5em }
                   , state/.style={ rectangle, draw=black, very thick,
                     minimum height=1.7em, minimum width = 4.4em, inner
                     sep=1pt, text centered, node distance = 1.5em },
                         *|/.style={
        to path={
            (perpendicular cs: horizontal line through={(\tikztostart)},
                                 vertical line through={(\tikztotarget)})
            % is the same as (\tikztostart -| \tikztotarget)
            % but just to be safe: http://tex.stackexchange.com/a/29781/16595
            -- (\tikztotarget) \tikztonodes
        }
    },
    |*/.style={to path={
        (\tikztostart) -- (perpendicular cs: vertical line through={(\tikztostart)},
                                             horizontal line through={(\tikztotarget)})
    }}
                   ]
  \node[state, minimum width = 6em] (code) {C++ code};
  \node[state, minimum width = 10.4em, right = 11em of code] (prop) {property and options};

  \node[state, below = 2.8em of code, rounded corners] (clang) {compiler};
  \node[state, below = 1.5em of clang.south west, anchor = north west] (runtime) {runtime};
  \node[state, right = 0.4 of clang] (llvm) {LLVM IR};
  \node[state, right = 0.4 of llvm, rounded corners, minimum width = 8em] (lart) {instrumentation};
  \node[state, right = 0.4 of lart] (illvm) {\divm IR};
  \node[state, right = 13.5em of runtime, rounded corners, minimum width = 8em] (verifier) {verification core};
  \node[above = 0.5em of lart] (pverify) {};

  \node[state, below = 1.7em of verifier.south east] (valid) {\color{greenf}Valid};
  \node[state, below = 1.7em of verifier.south west, minimum width = 8em] (ce) {\color{orangef}Counterexample};

  \begin{pgfonlayer}{background}
      \node[state, fit = (pverify) (clang) (runtime) (llvm) (lart) (illvm) (verifier),
            inner sep = 0.8em, thick, rounded corners, dashed] (verify) {};
  \end{pgfonlayer}

  \node[below = 0.2em] at (verify.north) {\texttt{divine verify}};

  \path (prop.346) edge[|*] (verifier.north)
        (prop.195) edge[|*] (lart.north)
        (code) edge (clang)
        (runtime) edge (clang)
        (clang) edge (llvm)
        (llvm) edge (lart)
        (lart) edge (illvm)
        (illvm) edge[|*] (verifier.north)
        (verifier) edge (valid) edge (ce)
        ;
\end{tikzpicture}
% }
\caption{
        \divine verification work-flow from ~\cite{Divine2}.
    }
  \label{fig:divine:translation}
\end{figure}

As shown in Fig.~\ref{fig:divine:translation},  the generalised workflow of \divine is as follows: \divine takes a C or C++ program and translates it with Clang compiler, a C language family frontend for LLVM~\cite{Clang}, into LLVM bitcode followed by instrumentation of this bitcode. This instrumented LLVM bitcode is then linked with the \divine-provided runtime libraries and verified against a given property.

Discovering an error, \divine produces a counter-example, providing information about the erroneous run and a backtrace from the state that the error occurred in, including line numbers.

\section{Environment}

Programs in \divine are executed by a virtual machine (called DiVM). The machine code executed by this virtual machine is an extension of the LLVM bitcode \cite{Divine}. DiVM supervises the control registers and manages memory allocations. The control registers are used to encode the current state of DiVM or to mark whether DiVM operates in the kernel mode. The memory of the program tested by \divine is stored in the form of a graph with additional information in the form of memory shadows. These shadows represent metadata of each allocated object including information about which bytes are initialised. 

As a result, DiVM is capable of performing memory management and control handling of interruptions for the verified program. 


\subsection{Operating System}

As \divine cannot execute a real operating system, it implements its POSIX-like operating system -- \divine’s operating system, \dios in short. 

DiOS is a verification-oriented operating system which provides a subset of the POSIX API to cover typical requirements of a program such as thread handling, file system access or scheduling. 

Primarily, \dios serves to provide a runtime environment -- scheduling routines which determine the order of thread execution, a fault handler and facilities for tracing errors. 

Since \dios runs inside DiVM, all interaction with the outside world must be simulated, because DiVM cannot perform any I/O operation. The aforementioned stems from the fact that verification of parallel programs in \divine consists of investigating all possible thread interleavings; therefore any communication with the outside world could lead to unreproducible behaviour in the verified program, since the effects of system calls cannot be easily replayed or undone.

This restriction is valid also with respect to the host file system. \dios cannot work with the host file system, as one interleaving could affect the result of another. Therefore, \dios uses a virtual file system which conforms to the file system semantics. 
 
\section{File System} \label{sec:divine:filesystem}


\divine's Virtual file system -- VFS, is a POSIX-compatible filesystem implementation provided by \dios, which does not propagate any operations performed by this VFS to the host~\cite{Divine}. 

 At the start of the verification process, the file system can be either empty or initialised by a snapshot provided by a user. This snapshot is captured before DiOS boots from a directory selected by the user. The file system provides a simple interface covering the functionality for many system calls.

During the verification process, the file system is not shared by various states of the state space of \divine. Rather, every state has its copy which is always forwarded to its successor. Note that this naturally allows system calls to perform actions directly on this file system. Additionally, this approach guarantees that no I/O operation is propagated to the outside world.

This file system support wide range of system calls, including fundamentals as \texttt{open}, \texttt{read}, \texttt{write}, \texttt{close} which are directly used by many programs, and also serve as the building blocks for C printing functions such as \texttt{printf} or C++ operators on streams. Additionally, it covers calls such as \texttt{stat}, \texttt{fstat} or \texttt{lseek} and services for sockets, namely \texttt{socketpair}, \texttt{getsockname}, \texttt{bind} or \texttt{connect}. 

Supported is also a significant part of header files \texttt{unistd.h,} \texttt{sys/socket.h} and \texttt{sys/stat.h}.

\section{\dios Modes}

\dios can be used in multiple configurations, which affect the environment in which the program under test runs. The available configurations are \texttt{default}, \texttt{passthrough}, \texttt{replay} and \texttt{synchronous}. The \texttt{default} provides an environment with asynchronous threads, processes and the already mentioned internal VFS implementation. The \texttt{synchronous} configuration brings a special environment useful for verification of synchronous systems. 

\subsection{Passthrough}

The \texttt{passthrough} approach allows DiOS to execute a system call in the outer operating system and preserve its input and output values for possible future analysis. The input and output values are preserved in the form of a trace, a binary file called \texttt{passthrough.out}. 

 There is naturally some limitation to the range of system calls that can be propagated this way. An example of a non-propagable system call is \texttt{fork}. The \texttt{fork} system call creates a new process by duplicating the calling process, which is \divine from the point of the view of the external operating system. For this reason, we want to avoid passing this syscall to the host operating system. 

\subsection{Replay}

The \texttt{replay} mode builds on the \texttt{passthrough} mode. The environment in the \texttt{replay} configuration is capable of loading and parsing the recorded trace \texttt{passthrough.out}. Then, upon a syscall invocation by the program, \dios is capable of simulating this system call using the parsed trace. Note that since all system calls are purely simulated, there is no real communication with the host operating system. 

\section{Execution Modes}

\divine provides several forms of execution, where each mode treats the verified program differently. 

\begin{figure}[h!]
\begin{minted}{bash}
[xkejstov@arke divine4]$ ./test/divine help
To print details about a specific command, run divine help {command}.

    verify [options] {file}
     check [options] {file}
      exec [options] {file}
       sim [options] {file}
      draw [options] {file}
      info [options] {file}
        cc [options]
   version [options]
      help [options]

\end{minted}
\caption{
       Obtaining execution modes of \divine using its help.
    }
  \label{fig:divineMode}
\end{figure}

The output in Fig. \ref{fig:divineMode} shows the main command line interface of \divine. Apart from the \texttt{help} command itself, program options list (\texttt{info}) and \texttt{version} listing, \divine provides these modes of executions: \texttt{verify} and \texttt{check}, \texttt{exec} standing for execute, \texttt{sim} for simulate, \texttt{draw} and \texttt{cc} standing for compile.  

\subsection{check, verify}

The check and verify modes perform model checking on a given program. While \texttt{verify} performs full model checking, the \texttt{check} command only covers most scenarios but does not guarantee full coverage. The  \texttt{check} mode can omit expensive checks or unlikely situations, such as \texttt{malloc} failures. 

Performing explicit model checking over a program and inspecting all possible thread executions and scenarios, we are forced to omit interaction with the real operating system. Primarily,  we cannot let the program execute system calls on the host operating system (including interaction with its file system). To achieve this behaviour,  \divine makes use of its already mentioned \dios file system implementation. 

\subsection{exec}

The execution mode of \divine runs the program picking one random thread interleaving. The assertions and memory safety of this one interleaving are still checked. Behaviour obtained by the execution mode can be compared to Valgrind -- an instrumentation framework for building dynamic analysis tools \cite{Valgrind}.

As the execution mode checks only one thread interleaving, the program can be allowed to directly communicate with the host operating system and its file system. This mode exploits the \texttt{passthrough} \dios configuration for dealing with system calls. 


\subsection{draw}

\divine operates over a state space of a program which is represented as an oriented graph. The vertices of the graph are states of the program including values in memory locations and registers. Edges intuitively represent possible transitions.

Many multi-threaded programs reach a point where more than one thread can run at once in parallel. Being in such a  state, the next state is determined by a scheduling choice. In \divine, this state will have more than one successor, since \divine replicates all possible scheduling choices.

 To visualise the state space, we can use the \texttt{divine draw} command listed in Fig. \ref{fig:divineMode}. 

The output of this mode is a state space graph reported in a \texttt{Dot} format -- a common graph description language. This output can be either converted to a PDF, or saved in the dot format and either further processed or visualised using external tools, such as online dot graph viewer Webgraphviz~\footnote{ \url{http://www.webgraphviz.com/} }. An example of such a state space representation is shown in \textit{Fig.} \ref{fig:stateSpace}.



\begin{figure}
\centering
\begin{minipage}[b]{.58\textwidth}
  % \centering
  \begin{lstlisting}[style=C,
    basicstyle=\footnotesize
  ]
#include <unistd.h>
#include <pthread.h>
int counter = 0;

void* fn(void* unused) {
  __dios_trace_f("Tread_entry");
  ++counter;
  __dios_trace_f("Thread_end");
  return NULL;
}

int main() {
  __dios_trace_f("MAIN_entry");
  pthread_t thread_b;
  pthread_create( &thread_b, NULL , fn, NULL);
  pthread_join( thread_b, NULL );
  __dios_trace_f("MAIN_end");
  return 0;
}
\end{lstlisting}
  % \subcaption{ (a) A simple C programm}
  % \label{fig:sub:program}
\end{minipage}%
\begin{minipage}[b]{.42\textwidth}
  % \centering
  \includegraphics[width=\linewidth]{img/main_graph.png}
  % \subcaption{ (b) A state space graph}
  % \label{fig:sub:statespace}
\end{minipage}
\caption{Example of a state space representation obtained with \divine draw execution mode. The \texttt{\_\_dios\_trace\_f} is an internal \divine logging function.}
\label{fig:stateSpace}
\end{figure}








